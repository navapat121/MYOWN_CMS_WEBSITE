﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace FR_CMS_WEBSITE.Class
{
    public class ContentTools
    {
        internal static bool saveHtml(string data, string path, string filename)
        {
            if (File.Exists(path + filename + ".htm"))
            {
                File.Delete(path + filename + ".htm");
            }

            File.WriteAllText(path + filename + ".htm", data);

            return true;
        }

        internal static bool updateSelectOption(string data, string filename)
        {
            //File.AppendAllText(DataFactory.CMS_FOLDER_SAVE_CONTENT_FILE_URL + "part\\" + filename + ".cshtml", data);
            //File.AppendAllText(DataFactory.MC_FOLDER_SAVE_CONTENT_FILE_URL + "part\\" + filename + ".cshtml", data);
            //File.AppendAllText(DataFactory.ECOM_FOLDER_SAVE_ADDITION_FILE_URL + "content\\assets\\part\\" + filename + ".cshtml", data);

            return true;
        }

        // save category's icon
        internal static bool saveCategoryIconImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".png"))
            {
                System.IO.File.Delete(path + filename + ".png");
            }

            Image squareImage = ResizeImage(image, new Size(40, 40));
            Bitmap img_cover = new Bitmap(squareImage, new Size(40, 40));// Scale image to 40x40
            img_cover.Save(path + filename + ".png", System.Drawing.Imaging.ImageFormat.Png);

            return true;
        }

        // save category's bg
        internal static bool saveCategoryBgImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".png"))
            {
                System.IO.File.Delete(path + filename + ".png");
            }

            Image squareImage = ResizeImage(image, new Size(144, 107));
            Bitmap img_cover = new Bitmap(squareImage, new Size(144, 107));// Scale image to 116x110
            img_cover.Save(path + filename + ".png", System.Drawing.Imaging.ImageFormat.Png);

            return true;
        }

        // save category's food
        internal static bool saveCategoryMobFoodImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(1200, 600));
            Bitmap img_cover = new Bitmap(squareImage, new Size(1200, 600));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save category's food
        internal static bool saveCategoryMobItemImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(600, 600));
            Bitmap img_cover = new Bitmap(squareImage, new Size(600, 600));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save cover's content
        internal static bool saveContentImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = ResizeImage(image, new Size(1000, 1000));
            Bitmap img_cover = new Bitmap(squareImage, new Size(1000, 1000));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        // save upload's content
        internal static bool saveContentUploadImage(Image image, string path, string filename)
        {
            if (File.Exists(path + filename + ".jpg"))
            {
                File.Delete(path + filename + ".jpg");
            }

            /*
            Size newSize = new Size(image.Width, image.Height);
            bool resize_height = false;
            bool resize_width = false;
            if (image.Width > 1000)
            {
                resize_width = true;
            }
            if (image.Height > 1000)
            {
                resize_height = true;
            }

            // if height > 400, resize height
            if (resize_height)
            {
                newSize.Height = 1000;
                newSize.Width = (int)(1000 * (image.Width / newSize.Height));
            }
            else if (resize_width)
            {
                newSize.Width = 1000;
                newSize.Height = (int)(1000 * (image.Height / image.Width));
            }
            */
            Bitmap img_content = new Bitmap(image, new Size(image.Width, image.Height));
            img_content.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        internal static bool saveCoverImage(Image image, string path, string filename)
        {
            if (System.IO.File.Exists(path + filename + ".jpg"))
            {
                System.IO.File.Delete(path + filename + ".jpg");
            }

            Image squareImage = null;
            if (image.Width == image.Height)
            {
                squareImage = image;
            }
            else
            {
                squareImage = ResizeImage(image, new Size(732,370)); 
            }
            Bitmap img_cover = new Bitmap(squareImage, new Size(732, 370));// Scale image to 128x128
            img_cover.Save(path + filename + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return true;
        }

        public static string RemoveLineEndings(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(lineSeparator, " ").Replace(paragraphSeparator, " ");
        }

        // Crop in center
        internal static Image ResizeImage(Image imgToResize, Size destinationSize)
        {
            var originalWidth = imgToResize.Width;
            var originalHeight = imgToResize.Height;

            //how many units are there to make the original length
            var hRatio = (float)originalHeight / destinationSize.Height;
            var wRatio = (float)originalWidth / destinationSize.Width;

            //get the shorter side
            var ratio = Math.Min(hRatio, wRatio);

            var hScale = Convert.ToInt32(destinationSize.Height * ratio);
            var wScale = Convert.ToInt32(destinationSize.Width * ratio);

            //start cropping from the center
            var startX = (originalWidth - wScale) / 2;
            var startY = (originalHeight - hScale) / 2;

            //crop the image from the specified location and size
            var sourceRectangle = new Rectangle(startX, startY, wScale, hScale);

            //the future size of the image
            var bitmap = new Bitmap(destinationSize.Width, destinationSize.Height);

            //fill-in the whole bitmap
            var destinationRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            //generate the new image
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, destinationRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }

            return bitmap;

        }
    }
}

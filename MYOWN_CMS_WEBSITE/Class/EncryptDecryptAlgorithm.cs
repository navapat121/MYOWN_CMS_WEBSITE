﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace MYOWN_CMS_WEBSITE.Class
{
    public static class EncryptDecryptAlgorithm
    {
        public static String EncryptData(String sdata, String key)
        {
            byte[] bdata = Encoding.UTF8.GetBytes(sdata);

            int nLen = bdata.Length;
            int nKeyLen = key.Length;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nLen; i++)
            {
                sb.Append(ToHex(((char)(bdata[i] ^ key[i % nKeyLen])).ToString()));
            }

            return sb.ToString();

        }

        private static String ToHex(String data)
        {
            StringBuilder sb = new StringBuilder();
            int nCh = (int)(data[0]);
            sb.Append((char)(((nCh >> 4) & 15) + 0x41));
            sb.Append((char)((nCh & 15) + 0x41));
            return sb.ToString();
        }

        public static String DecryptData(String data, String key)
        {
            try
            {
                int nLen = data.Length;
                int nKeyLen = key.Length;

                byte[] bdata = new byte[nLen / 2];
                int nCount = 0;
                for (int i = 0; i < nLen; i += 2)
                {
                    byte byteData = ToByte("" + data[i] + data[i + 1]);
                    bdata[nCount] = (byte)(byteData ^ key[nCount % nKeyLen]);
                    nCount++;
                }
                return Encoding.UTF8.GetString(bdata);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static byte ToByte(String data)
        {
            return (byte)((((int)data[0] - 0x41) << 4) | ((int)data[1] - 0x41));
        }
    }
}
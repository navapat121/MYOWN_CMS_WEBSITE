﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MYOWN_CMS_WEBSITE.Class;
using System.Web;
using System.Net.Http;
using System;



namespace MYOWN_CMS_WEBSITE.Controllers
{
    public partial class SigninController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            clearCookie();
            return RedirectToAction("Login", "Signin");
        }

        [HttpPost]
        public ActionResult Authentication(string username, string password)
        {
            var client = new HttpClient();

            var response = client.PostAsJsonAsync(DataFactory.SUBMIT_SIGNIN_AUTHENTICATION_API,
                EncryptDecryptAlgorithm.EncryptData(new JavaScriptSerializer().Serialize(new object[1] {
                new {
                    u = username,
                    p = password,
                }}), DataFactory.ENC_SIGNINKEY)).Result;
            
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                if (result.StartsWith(DataFactory.CODE_USERID)){

                    string []data = result.Split(';');

                    ViewBag.User = data[1];
                    ViewBag.data = data[2];

                    HttpCookie cookie1 = new HttpCookie(DataFactory.COOKIE_UID);
                    cookie1.Value = data[1];
                    cookie1.Expires = DateTime.Today.AddDays(1);
                    Response.Cookies.Add(cookie1);

                    HttpCookie cookie2 = new HttpCookie(DataFactory.COOKIE_UNAME);
                    cookie2.Value = HttpUtility.UrlEncode(data[2]);
                    cookie2.Expires = DateTime.Today.AddDays(1);
                    Response.Cookies.Add(cookie2);

                    return RedirectToAction("Index", "Home");
                }else if (result.Equals("NOTFOUND"))
                {
                    clearCookie();
                    string[] error = result.Split(':');
                    return RedirectToAction("Login", "Signin",
                        new
                        {
                            err = "Username or Password is not correct"
                        });
                }
                clearCookie();

                return RedirectToAction("Login", "Signin",
                    new
                    {
                        err = "unexpected error, please try again. error:" + result
                    });
            }
            else
            {
                clearCookie();

                return RedirectToAction("Login", "Signin", new
                {
                    errMsg = "Request timeout, please signin again."
                });
            }
        }

        public void clearCookie()
        {
            HttpCookie cookie1 = new HttpCookie(DataFactory.COOKIE_UID);
            cookie1.Expires = DateTime.Today.AddDays(-365);
            Response.Cookies.Add(cookie1);

            HttpCookie cookie2 = new HttpCookie(DataFactory.COOKIE_UNAME);
            cookie2.Expires = DateTime.Today.AddDays(-365);
            Response.Cookies.Add(cookie2);

            //HttpCookie cookie3 = new HttpCookie(DataFactory.COOKIE_UIMG);
            //cookie3.Expires = DateTime.Today.AddDays(-365);
            //Response.Cookies.Add(cookie3);
        }
    }
}
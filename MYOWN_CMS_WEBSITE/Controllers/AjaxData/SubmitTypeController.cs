﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MYOWN_CMS_WEBSITE.Class;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MYOWN_CMS_WEBSITE.Controllers.AjaxData
{
    public partial class AjaxDataController : Controller
    {
        #region SubmitType
        [HttpPost]
        public String SubmitType(string name_na, string name_en, string description)
        {
            var client = new HttpClient();

            var response_save = client.PostAsJsonAsync(DataFactory.SUBMIT_DRINK_TYPE, EncryptDecryptAlgorithm.EncryptData(
            JsonConvert.SerializeObject(new Object[1]{
                new
                    {
                        name_na = name_na,
                        name_en = name_en,
                        description = description,

                    }}), DataFactory.ENC_ADKEY)).Result;

            if (response_save.IsSuccessStatusCode)
            {
                var result = response_save.Content.ReadAsStringAsync().Result;
                ViewBag.result = result;
                return result;
            }
            else
            {
                return "ERROR:003";
            }
        }
        #endregion
    }
}
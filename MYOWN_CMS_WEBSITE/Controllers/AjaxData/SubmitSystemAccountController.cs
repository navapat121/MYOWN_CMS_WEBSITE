﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MYOWN_CMS_WEBSITE.Class;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MYOWN_CMS_WEBSITE.Controllers.AjaxData
{
    public partial class AjaxDataController : Controller
    {
        #region SubmitSystemAccount
        [HttpPost]
        public String SubmitSystemAccount(string username, string password, string f_name, string l_name, string telephone, string email)
        {
            var client = new HttpClient();

            var response_save = client.PostAsJsonAsync(DataFactory.SUBMIT_SYSTEM_ACCOUNT, EncryptDecryptAlgorithm.EncryptData(
            JsonConvert.SerializeObject(new Object[1]{
                new
                    {
                        username = username,
                        password = password,
                        f_name = f_name,
                        l_name = l_name,
                        telephone = telephone,
                        email = email,

                    }}), DataFactory.ENC_ADKEY)).Result;

            if (response_save.IsSuccessStatusCode)
            {
                var result = response_save.Content.ReadAsStringAsync().Result;
                ViewBag.result = result;
                return result;
            }
            else
            {
                return "ERROR:003";
            }
        }
        #endregion
    }
}
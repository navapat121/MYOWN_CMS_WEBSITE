﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MYOWN_CMS_WEBSITE.Class;

namespace MYOWN_CMS_WEBSITE.Controllers
{
    public partial class SystemAccountController : Controller
    {
        public ActionResult NewSystemAccount(int? id)
        {
            if (id == null) id = 0;

            if (id != 0)
            {
                var client = new HttpClient();
                var response = client.PostAsJsonAsync(DataFactory.REQUEST_SYSTEM_ACCOUNT_API,
                    EncryptDecryptAlgorithm.EncryptData(new JavaScriptSerializer().Serialize(new Object[1]{
                        new {
                                id = id,
                    }}), DataFactory.ENC_ADKEY)).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result.StartsWith(DataFactory.CODE_ERROR))
                    {
                        string[] error = result.Split(':');
                        ViewBag.error = error[1];
                        return View();
                    }
                    ViewBag.result = result;
                }
                else
                {
                    ViewBag.err = "ERROR:003";
                }

            }

            ViewBag.id = id;
            return View();
        }
    }
}
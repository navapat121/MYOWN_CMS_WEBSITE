﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace MYOWN_CMS_WEBSITE.Class
{
    public class DataFactory
    {
        // ============ API Server ============== //
        public static readonly string MYOWN_API_URL = ConfigurationManager.AppSettings["MYOWN_API_URL"];

        // ============ Content Upload ============ //
        public static readonly string URL_MYOWN_CONTENT = MYOWN_API_URL + "content/";
        public static readonly string URL_MYOWN_CONTENT_UPLOAD = URL_MYOWN_CONTENT + "upload/";

        // ===== Cookie ======
        public static readonly string COOKIE_UID = "myown_cms_u_id";
        public static readonly string COOKIE_UNAME = "myown_cms_u_name";

        // ============== Submit Data ================ //
        public static readonly string SUBMIT_SYSTEM_ACCOUNT = MYOWN_API_URL + "api/SubmitSystemAccount/";
        public static readonly string SUBMIT_DRINK_TYPE = MYOWN_API_URL + "api/SubmitType/";
        public static readonly string SUBMIT_DRINK_BRAND = MYOWN_API_URL + "api/SubmitBrand/";
        

        // ====== Web Admin Auth ======
        internal static readonly string ENC_ADKEY = "qF#!kKz@alf3)";
        internal static readonly string ENC_SIGNINKEY = "qF#!kKz@alf3)";

        internal static readonly string SUBMIT_SIGNIN_AUTHENTICATION_API = MYOWN_API_URL + "api/SigninAuthentication/";
        internal static readonly string REQUEST_SYSTEM_ACCOUNT_API = MYOWN_API_URL + "api/RequestSystemAccount/";

        // ====== web template =========
        internal static readonly string CODE_SUCCESS = "SUCCESS;";
        internal static readonly string CODE_USERID = "USERID;";
        internal static readonly string CODE_ERROR = "ERROR;";
        internal static readonly string CODE_RESULTID = "RESULTID;";

    }
}